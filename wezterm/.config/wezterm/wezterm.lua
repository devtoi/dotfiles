local wezterm = require "wezterm"
return {
	-- keys = {
	-- 	{ key = "i", mods = "CTRL", action = wezterm.action({ SendString = "\x0e69" }) },
	-- },
	debug_key_events = true,
	enable_csi_u_key_encoding = true,
	font = wezterm.font_with_fallback({
		"Hack Nerd Font Mono",
		-- "Hack",
		-- "Fira Code"
	}),
	color_scheme = "MaterialDarker",
	enable_wayland = true,
	scrollback_lines = 10000,
	enable_scrollbar = true,
}
