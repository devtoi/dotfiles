[
 (type_identifier)
 (sized_type_specifier)
 (type_descriptor)
] @type

[
 (primitive_type)
 (nullptr)
] @type.builtin

((identifier) @constant
 (#match? @constant "(^_.*_$)"))
