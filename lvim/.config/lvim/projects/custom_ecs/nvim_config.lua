lvim.builtin.dap.on_config_done = function(dap)
	dap.adapters.lldb = {
		type = "executable",
		command = "/usr/bin/lldb-vscode",
		name = "lldb",
	}

	dap.configurations.cpp = {
		{
			name = "Launch",
			type = "lldb",
			request = "launch",
			program = "${workspaceFolder}/build/bin/custom_ecs",
			cwd = "${workspaceFolder}/build/bin",
			stopOnEntry = false,
			args = {},
			runInTerminal = false,
		},
	}
	dap.configurations.c = dap.configurations.cpp

	-- dap.defaults.fallback.external_terminal = {
	-- 	command = '/usr/bin/wezterm',
	-- 	args = { '-e' },
	-- }
	-- dap.defaults.fallback.force_external_terminal = true
end

local build_settings = {
	cmd = [[
		../compile_shaders.sh ../src/shader shader
		ninja
		echo "Press enter..."
		read
		]],
	dir = "build",
}

require("null-ls").builtins.formatting.clang_format.command = "/home/toi/proj/external_libs3/common/clang-format/linux/clang-format"
local formatters = require "lvim.lsp.null-ls.formatters"
formatters.setup {
  {
	exe = "clang_format",
	command = "/home/toi/proj/external_libs3/common/clang-format/linux/clang-format",
	filetypes = { "cpp", "h" },
  },
}

local callback = lvim.builtin.which_key.on_config_done
lvim.builtin.which_key.on_config_done = function(wk)
	if callback then
		callback(wk)
	end
	local opts = {
		mode = "n", -- NORMAL mode
		prefix = "", -- "<leader>",
		buffer = nil, -- Global mappings. Specify a buffer number for buffer local mappings
		silent = true, -- use `silent` when creating keymaps
		noremap = true, -- use `noremap` when creating keymaps
		nowait = true, -- use `nowait` when creating keymaps
	}
	local mappings = {
		["<F7>"] = {
			function()
				BuildProject(build_settings)
			end,
			"Build",
		},
		["<F5>"] = {
			function()
				DebugProject(build_settings)
			end,
			"Debug",
		},
	}
	wk.register(mappings, opts)
end
