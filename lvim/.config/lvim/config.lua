-- vim options
vim.opt.wrap = true
vim.opt.timeoutlen = 300
vim.opt.shiftwidth = 4
vim.opt.tabstop = 4
vim.opt.expandtab = false
vim.opt.relativenumber = true

-- lvim plugin toggles
lvim.builtin.alpha.active = false
lvim.builtin.autopairs.active = false
lvim.builtin.dap.active = true
lvim.builtin.gitsigns.active = true
lvim.builtin.lualine.active = true
lvim.builtin.telescope.active = true
lvim.builtin.terminal.active = true
lvim.builtin.which_key.active = true

-- general
lvim.log.level = "info"
lvim.format_on_save = {
	enabled = true,
	pattern = "*.lua,*.cpp,*.h,*.py",
	timeout = 5000,
}

-- keymappings <https://www.lunarvim.org/docs/configuration/keybindings>
lvim.leader = "space"
lvim.keys.insert_mode["Jk"] = "<Esc>"
lvim.keys.insert_mode["jk"] = "<Esc>"
lvim.keys.insert_mode.kj = nil
lvim.keys.normal_mode[";"] = ":"

-- Theme
lvim.colorscheme = "kanagawa-wave"
vim.o.background = ''
require("kanagawa").load("wave")

-- Automatically install missing parsers when entering buffer
lvim.builtin.treesitter.auto_install = true
lvim.builtin.treesitter.ensure_installed = { "comment", "markdown_inline", "regex", "cpp" }

-- Terminal
lvim.builtin.terminal.size = 10

lvim.builtin.dap.ui.config.layouts = {
	{
		elements = {
			{ id = "scopes",      size = 0.33 },
			{ id = "breakpoints", size = 0.17 },
			{ id = "stacks",      size = 0.25 },
			{ id = "watches",     size = 0.25 },
		},
		size = 0.33,
		position = "right",
	},
	{
		elements = {
			{ id = "repl", size = 1.0 },
			--{ id = "console", size = 0.55 },
		},
		size = 0.27,
		position = "bottom",
	},
}

--lvim.builtin.which_key.mappings["s"]["T"] = {
--	"<cmd>lua require('telescope').extensions.live_grep_args.live_grep_args()<CR>", "Live grep raw"
--}

local notify_og = vim.notify
vim.notify = function(msg, ...)
	if msg:match("warning: multiple different client offset_encodings") then
		return
	end

	notify_og(msg, ...)
end


--lvim.auto_close_tree = 0

-- lvim.lsp.null_ls.setup.on_init = function(new_client, _)
-- 	new_client.offset_encoding = 'utf-32'
-- end

lvim.builtin.treesitter.highlight.enable = true
lvim.builtin.treesitter.textobjects.select.enable = true
lvim.builtin.treesitter.textobjects.swap.enable = true
lvim.builtin.treesitter.textsubjects.enable = true
lvim.builtin.treesitter.rainbow.enable = true
lvim.builtin.treesitter.indent.enable = true
lvim.builtin.treesitter.context_commentstring.enable = true
lvim.builtin.treesitter.autotag.enable = true

lvim.builtin.telescope.defaults.initial_mode = "insert"
lvim.builtin.telescope.defaults.layout_strategy = "horizontal"
lvim.builtin.telescope.defaults.layout_config.horizontal = {}
lvim.builtin.telescope.defaults.layout_config.horizontal.mirror = false
lvim.builtin.telescope.defaults.layout_config.vertical = {}
lvim.builtin.telescope.defaults.layout_config.vertical.mirror = false
lvim.builtin.telescope.defaults.layout_config.preview_cutoff = 120
lvim.builtin.telescope.defaults.layout_config.prompt_position = "bottom"
lvim.builtin.telescope.defaults.layout_config.width = 0.95
lvim.builtin.telescope.defaults.layout_config.height = 0.95

-- On entering insert mode in any file, scroll the window so the cursor line is centered
vim.api.nvim_create_autocmd("BufReadPost", { pattern = "*", command = ":normal! g`\"zvzz" })

-- Additional Plugins
lvim.plugins = {
	{ "rebelot/kanagawa.nvim" },
	{ "nvim-treesitter/nvim-treesitter-textobjects" },
	{ "nvim-treesitter/nvim-treesitter-refactor" },
	{ "nvim-treesitter/nvim-treesitter-context" },
	{
		"ray-x/lsp_signature.nvim",
		config = function()
			require("lsp_signature").on_attach()
		end,
		event = "InsertEnter",
	},
	{ "mhinz/vim-startify" },
	--{ "p00f/clangd_extensions.nvim" },
	{ url = "https://gitlab.com/devtoi/init_more.nvim" },
	--{ "theHamsta/nvim-dap-virtual-text" },
	--{ 'nvim-telescope/telescope-live-grep-raw.nvim' },
	{ 'stevearc/dressing.nvim' },
	{ 'barreiroleo/ltex-extra.nvim' },
	{
		"SmiteshP/nvim-navic",
		dependencies = "neovim/nvim-lspconfig",
		opts = {
			lsp = {
				auto_attach = true
			}
		}
	},
	{
		'salkin-mada/openscad.nvim',
		config = function()
			require('openscad')
			-- load snippets, note requires
			vim.g.openscad_load_snippets = true
		end,
		dependencies = 'L3MON4D3/LuaSnip'
	}
}

lvim.lsp.installer.setup.ensure_installed = { "clangd", "beancount", "cmake", "ltex", "lua_ls", "marksman",
	"openscad_lsp", "pylsp", "rust_analyzer", "texlab" }

require("lvim.lsp.manager").setup("beancount", {
	init_options = {
		journal_file = "/home/toi/economy/personal.beancount",
	},
	-- journal_file = "/home/toi/economy/personal.beancount",
	-- cmd = { "beancount-language-server" },
	-- filetypes = { "beancount" },
})

require("lvim.lsp.manager").setup("texlab", {
	settings = {
		texlab = {
			build = {
				args = { "-pdf", "-interaction=nonstopmode", "-synctex=1", "-pvc", "%f" },
			},
			forwardSearch = {
				executable = "okular",
				args = { "--unique", "file:%p#src:%l%f" }
			}
		}
	}
})

require("lvim.lsp.manager").setup("ltex", {
	on_attach = function(client, bufnr)
		-- your other on_attach functions.
		require("ltex_extra").setup {
			load_langs = { "en-US" }, -- table <string> : languages for witch dictionaries will be loaded
			init_check = true, -- boolean : whether to load dictionaries on startup
			path = nil,      -- string : path to store dictionaries. Relative path uses current working directory
			log_level = "none", -- string : "none", "trace", "debug", "info", "warn", "error", "fatal"
		}
	end,
	cmd = { "ltex-ls" },
	flags = { debounce_text_changes = 300 },
	settings = {
		ltex = {
			language = "en-US"
		}
	},
})

--require("clangd_extensions").setup {
--	server = {
--		-- options to pass to nvim-lspconfig
--		-- i.e. the arguments to require("lspconfig").clangd.setup({})
--	},
--	extensions = {
--		-- defaults:
--		-- Automatically set inlay hints (type hints)
--		autoSetHints = true,
--		-- Whether to show hover actions inside the hover window
--		-- This overrides the default hover handler
--		hover_with_actions = true,
--		-- These apply to the default ClangdSetInlayHints command
--		inlay_hints = {
--			-- Only show inlay hints for the current line
--			only_current_line = false,
--			-- Event which triggers a refersh of the inlay hints.
--			-- You can make this "CursorMoved" or "CursorMoved,CursorMovedI" but
--			-- not that this may cause  higher CPU usage.
--			-- This option is only respected when only_current_line and
--			-- autoSetHints both are true.
--			only_current_line_autocmd = "CursorHold",
--			-- whether to show parameter hints with the inlay hints or not
--			show_parameter_hints = true,
--			-- whether to show variable name before type hints with the inlay hints or not
--			show_variable_name = false,
--			-- prefix for parameter hints
--			parameter_hints_prefix = "<- ",
--			-- prefix for all the other hints (type, chaining)
--			other_hints_prefix = "=> ",
--			-- whether to align to the length of the longest line in the file
--			max_len_align = false,
--			-- padding from the left if max_len_align is true
--			max_len_align_padding = 1,
--			-- whether to align to the extreme right or not
--			right_align = false,
--			-- padding from the right if right_align is true
--			right_align_padding = 7,
--			-- The color of the hints
--			highlight = "Comment",
--		},
--		ast = {
--			role_icons = {
--				type = "",
--				declaration = "",
--				expression = "",
--				specifier = "",
--				statement = "",
--				["template argument"] = "",
--			},
--			kind_icons = {
--				Compound = "",
--				Recovery = "",
--				TranslationUnit = "",
--				PackExpansion = "",
--				TemplateTypeParm = "",
--				TemplateTemplateParm = "",
--				TemplateParamObject = "",
--			},
--			highlights = {
--				detail = "Comment",
--			},
--		}
--	}
--}

require('dressing').setup({
	input = {
		-- Set to false to disable the vim.ui.input implementation
		enabled = true,
		-- Default prompt string
		default_prompt = "➤ ",
		-- Can be 'left', 'right', or 'center'
		prompt_align = "left",
		-- When true, <Esc> will close the modal
		insert_only = true,
		-- These are passed to nvim_open_win
		anchor = "SW",
		border = "rounded",
		-- 'editor' and 'win' will default to being centered
		relative = "cursor",
		-- These can be integers or a float between 0 and 1 (e.g. 0.4 for 40%)
		prefer_width = 40,
		width = nil,
		-- min_width and max_width can be a list of mixed types.
		-- min_width = {20, 0.2} means "the greater of 20 columns or 20% of total"
		max_width = { 140, 0.9 },
		min_width = { 20, 0.2 },
		win_options = {
			-- Window transparency (0-100)
			winblend = 10,
			-- Change default highlight groups (see :help winhl)
			winhighlight = ""
		},
		override = function(conf)
			-- This is the config that will be passed to nvim_open_win.
			-- Change values here to customize the layout
			return conf
		end,
		-- see :help dressing_get_config
		get_config = nil,
	},
	select = {
		-- Set to false to disable the vim.ui.select implementation
		enabled = true,
		-- Priority list of preferred vim.select implementations
		backend = { "nui", "builtin", "fzf_lua", "fzf", "telescope" },
		-- Options for telescope selector
		-- These are passed into the telescope picker directly. Can be used like:
		-- telescope = require('telescope.themes').get_ivy({...})
		telescope = nil,
		-- Options for fzf selector
		fzf = {
			window = {
				width = 0.5,
				height = 0.4,
			},
		},
		-- Options for fzf_lua selector
		fzf_lua = {
			winopts = {
				width = 0.5,
				height = 0.4,
			},
		},
		-- Options for nui Menu
		nui = {
			position = "50%",
			size = nil,
			relative = "editor",
			border = {
				style = "rounded",
			},
			max_width = 80,
			max_height = 40,
		},
		-- Options for built-in selector
		builtin = {
			-- These are passed to nvim_open_win
			anchor = "NW",
			border = "rounded",
			-- 'editor' and 'win' will default to being centered
			relative = "editor",
			win_options = {
				-- Window transparency (0-100)
				winblend = 10,
				-- Change default highlight groups (see :help winhl)
				winhighlight = "",
			},
			-- These can be integers or a float between 0 and 1 (e.g. 0.4 for 40%)
			-- the min_ and max_ options can be a list of mixed types.
			-- max_width = {140, 0.8} means "the lesser of 140 columns or 80% of total"
			width = nil,
			max_width = { 140, 0.8 },
			min_width = { 40, 0.2 },
			height = nil,
			max_height = 0.9,
			min_height = { 10, 0.2 },
			override = function(conf)
				-- This is the config that will be passed to nvim_open_win.
				-- Change values here to customize the layout
				return conf
			end,
		},
		-- Used to override format_item. See :help dressing-format
		format_item_override = {},
		-- see :help dressing_get_config
		get_config = nil,
	},
})

-- require("nvim-dap-virtual-text").setup {
-- 	enabled = true,                  -- enable this plugin (the default)
-- 	enabled_commands = true,         -- create commands DapVirtualTextEnable, DapVirtualTextDisable, DapVirtualTextToggle, (DapVirtualTextForceRefresh for refreshing when debug adapter did not notify its termination)
-- 	highlight_changed_variables = true, -- highlight changed values with NvimDapVirtualTextChanged, else always NvimDapVirtualText
-- 	highlight_new_as_changed = false, -- highlight new variables in the same way as changed variables (if highlight_changed_variables)
-- 	show_stop_reason = true,         -- show stop reason when stopped for exceptions
-- 	commented = false,               -- prefix virtual text with comment string
-- 	-- experimental features:
-- 	virt_text_pos = 'eol',           -- position of virtual text, see `:h nvim_buf_set_extmark()`
-- 	all_frames = false,              -- show virtual text for all stack frames not only current. Only works for debugpy on my machine.
-- 	virt_lines = false,              -- show virtual lines instead of virtual text (will flicker!)
-- 	virt_text_win_col = nil          -- position the virtual text at a fixed window column (starting from the first text column) ,
-- 	-- e.g. 80 to position at column 80, see `:h nvim_buf_set_extmark()`
-- }

--require("dapui").setup({
--	icons = {
--		expanded = "▾",
--		collapsed = "▸",
--	},
--	mappings = {
--		-- Use a table to apply multiple mappings
--		expand = { "<CR>", "<2-LeftMouse>" },
--		open = "o",
--		remove = "d",
--		edit = "e",
--		repl = "r",
--	},
--	layouts = {
--		{
--			elements = {
--				'scopes',
--				'breakpoints',
--				'stacks',
--				'watches',
--			},
--			size = 40,
--			position = 'left',
--		},
--		{
--			elements = {
--				'repl',
--				'console',
--			},
--			size = 10,
--			position = 'bottom',
--		},
--	},
--	floating = {
--		max_height = nil, -- These can be integers or a float between 0 and 1.
--		max_width = nil, -- Floats will be treated as percentage of your screen.
--	},
--})

local dap, dapui = require('dap'), require('dapui')
dap.listeners.after.event_initialized['dapui'] = function() dapui.open() end
dap.listeners.before.event_terminated['dapui'] = function()
	dapui.close()
end
dap.listeners.before.event_exited['dapui'] = function()
	dapui.close()
end

local quickfix_entries = {}

local function on_stdout(_, _, data, _)
	for _, s in ipairs(data) do
		table.insert(quickfix_entries, s)
	end
end

local function quick_fix_has_errors()
	local qfs = vim.fn.getqflist()

	local found_error = false

	for _, s in ipairs(qfs) do
		if s.type == "e" then
			found_error = true
			break
		end
	end

	return found_error, qfs
end

local efm
local function on_exit(on_build_done)
	vim.fn.setqflist({}, " ", {
		lines = quickfix_entries,
		efm = efm,
	})

	local found_error, qfs = quick_fix_has_errors()

	if #qfs > 0 then
		vim.defer_fn(function()
			vim.cmd("copen")
			if quick_fix_has_errors() then
				vim.cmd("cc")
			end
		end, 20)
	end

	if on_build_done then
		on_build_done(found_error)
	end
end

BuildProject = function(settings, on_build_done)
	efm = vim.api.nvim_get_option("errorformat")

	quickfix_entries = {}
	vim.cmd([[:wa]])
	local Terminal = require("toggleterm.terminal").Terminal
	local build = Terminal:new({
		cmd = settings.cmd,
		hidden = false,
		dir = settings.dir,
		on_stderr = on_stdout,
		on_stdout = on_stdout,
		direction = "horizontal",
		on_exit = function()
			on_exit(on_build_done)
		end,
	})
	build:toggle()
end

local on_build_done = function(failure)
	vim.schedule(function()
		if not failure then
			require("dap").continue()
		end
	end)
end

DebugProject = function(settings)
	BuildProject(settings, on_build_done)
end

lvim.builtin.which_key.on_config_done = function(module)
	module.register({
		s = {
			name = "Search",
			t = {
				-- Has to be a cmd with : in it to properly exist visual mode
				":lua SearchSelected()<CR>",
				"Search Selected Text",
			},
		},
	}, {
		mode = "v",
		prefix = "<leader>",
	})

	module.register({
		M = {
			function()
				require("dapui").eval()
			end,
			"Evaluate selected expression",
		},
	}, {
		mode = "v",
	})
	module.register({
		M = {
			function()
				require("dapui").eval()
			end,
			"Evaluate selected expression",
		},
		["<leader>k"] = {
			"<cmd>ClangdSwitchSourceHeader<CR>",
			"Toggle h/cpp",
		},
	}, {
		mode = "n",
	})

	module.register({
		["dq"] = {
			function()
				local dap = require("dap")
				local dapui = require("dapui")
				dap.disconnect({ restart = false, terminateDebuggee = true })
				dapui.close()
				dap.close()
			end,
			"Quit"
		},
	}, {
		mode = "n",
		prefix = "<leader>",
	})
end

local im = require("init_more")

im.silent = true
-- Poses potential security risks
-- im.options.load_from_current_directory = true
im.options.load_vim_config = true
im.options.projects_directory = "~/.config/lvim/projects/"

im.load_project_config()

--vim.g.indent_blankline_char = "│"
--vim.g.indent_blankline_space_char_blankline = " "
--vim.g.indent_blankline_use_treesitter = true
--vim.g.indent_blankline_show_first_indent_level = false
--vim.g.indent_blankline_show_current_context = true
--vim.g.indent_blankline_context_patterns = {
--	"class",
--	"function",
--	"method",
--	"function_declaration",
--	"struct_specifier",
--	"if_statement",
--	"for_range_loop",
--	"switch_statement",
--	"while_statement",
--}
function SetFTCompiler()
	-- Have to use schedule for some reason
	vim.schedule(function()
		-- vim.bo.commentstring = "// %s"
		vim.g.compiler_clang_ignore_unmatched_lines = true
		vim.cmd([[compiler! clang]])
		vim.cmd([[compiler clang]])
	end)
end

-- au BufNewFile,BufRead *.cpp lua vim.schedule(function() SetFTCompiler() end)
vim.cmd([[
augroup compile
" Remove all vimrc autocommands
autocmd!
au FileType cpp lua SetFTCompiler()
augroup END
]])

local function visual_selection_range()
	local _, csrow, cscol, _ = unpack(vim.fn.getpos("'<"))
	local _, cerow, cecol, _ = unpack(vim.fn.getpos("'>"))
	return csrow - 1, cscol - 1, cerow - 1, cecol
end

local function get_selected_text()
	local start_row, start_column, end_row, end_column = visual_selection_range()

	local lines = vim.api.nvim_buf_get_lines(0, start_row, end_row + 1, true)
	if lines then
		local length = #lines
		if length == 1 then
			lines[length] = lines[length]:sub(start_column + 1, end_column)
		else
			lines[1] = lines[1]:sub(start_column + 1, #lines[1])
			lines[length] = lines[length]:sub(0, end_column)
		end
	else
		return ""
	end
	local selected_text = ""
	for _, line in ipairs(lines) do
		selected_text = selected_text .. line
	end
	return selected_text
end

function SearchSelected(opts)
	local mode = vim.api.nvim_get_mode()
	-- Hack to exit visual mode
	if mode.mode == "v" then
		vim.fn.feedkeys(vim.api.nvim_replace_termcodes("<Esc>", true, false, true))
	end

	local selected_text = get_selected_text()
	local telescope = require("telescope.builtin")
	local lg_opts = opts or {
		search_dirs = { "./" },
		default_text = selected_text,
		initial_mode = "insert",
	}
	telescope.live_grep(lg_opts)
end
