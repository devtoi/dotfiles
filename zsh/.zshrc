#export QT_QPA_PLATFORMTHEME=qt5ct
#QT_QPA_PLATFORMTHEME=qt5ct
##export _JAVA_AWT_WM_NONREPARENTING=1
##_JAVA_AWT_WM_NONREPARENTING=1
##export SDL_VIDEODRIVER=wayland
#export NeovideMultiGrid=1
#export PATH=/home/$USER/.config/nvim/utils/bin:/home/$USER/bin:/home/$USER/.local/bin:$PATH
#export WLR_RENDERER=vulkan

# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
# if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
#   source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
# fi

alias lv=lvim
alias lg=lazygit
alias ll="ls -al"
alias llt="ls -al --sort=time"
alias lls="ls -al --sort=size"

ZSH=/usr/share/oh-my-zsh/
export DEFAULT_USER="toi"
export TERM="xterm-256color"
#export ZSH=/usr/share/oh-my-zsh
#export ZSH_POWER_LEVEL_THEME=/usr/share/zsh-theme-powerlevel10k

#source $ZSH_POWER_LEVEL_THEME/powerlevel10k.zsh-theme
source /usr/share/zsh/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh
source /usr/share/zsh/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

#ZSH_THEME="powerlevel10k"
#ZSH_THEME="random"

plugins=(
  git
  archlinux
  vscode
  colorize
  colored-man-pages
  command-not-found
  cp
  sudo
  bundler
  dotenv
)

#source $ZSH/oh-my-zsh.sh
#ZSH_CACHE_DIR=$HOME/.cache/oh-my-zsh
#if [[ ! -d $ZSH_CACHE_DIR ]]; then
#  mkdir $ZSH_CACHE_DIR
#fi

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=10000
bindkey -v
#bindkey <S-BS>
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/toi/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall

if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
	  exec sway
fi

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
#[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh
eval "$(oh-my-posh init zsh --config ~/.config/ohmyposh/powerlevel10k_modern.omp.json)"
