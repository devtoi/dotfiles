
export QT_QPA_PLATFORMTHEME=qt5ct
QT_QPA_PLATFORMTHEME=qt5ct
#export _JAVA_AWT_WM_NONREPARENTING=1
#_JAVA_AWT_WM_NONREPARENTING=1
#export SDL_VIDEODRIVER=wayland
export NeovideMultiGrid=1
export VK_ICD_FILENAMES="/usr/share/vulkan/icd.d/radeon_icd.i686.json:/usr/share/vulkan/icd.d/radeon_icd.x86_64.json"
export AMD_VULKAN_ICD=RADV
export PATH=/home/$USER/.config/nvim/utils/bin:/home/$USER/bin:/home/$USER/.local/bin:$PATH
export CC=/usr/bin/clang
export CXX=/usr/bin/clang++
export EDITOR=nvim
export XDG_SESSION_TYPE=wayland
export XDG_CURRENT_DESKTOP=sway
export MOZ_ENABLE_WAYLAND=1
export VDPAU_DRIVER=radeonsi
export LIBVA_DRIVER_NAME=radeonsi

